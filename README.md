# hampelsoft-libraries

[![coverage](https://gitlab.com/gitlab-org/gitlab-ee/badges/master/coverage.svg?job=coverage)](https://gitlab-org.gitlab.io/gitlab-ee/coverage-ruby)


## :memo: Description 

The hse-libraries are a collection of useful VIs developed over the years for and within our real-life projects. The hse-libraries form the basis of all our applications, similar to a very basic framework.



## :camera: Screenshot 

tbd.


## :rocket: Installation

The latest versions can be found at 
https://dokuwiki.hampel-soft.com/projects/hampel-soft/hse/hse-libraries/releases

Just extract the release archive to your LabVIEW project directory. 

## :bulb: Usage

The hse-libraries come with the following tools:
* hse-application
* hse-configuration
* hse-db
* hse-dqmh
* hse-gennet
* hse-lib
* hse-logger

If you want to use the framework helpers (hse-application and hse-configuration), you can find more information about our file and project structure at
https://dokuwiki.hampel-soft.com/kb/hampel-soft/project-structure

hse-db contains a Database DQMH module and a few database engines/drivers

hse-dqmh contains helpers for loading and using DQMH modules. More details about this can be found at 
https://gitlab.com/hampel-soft/dqmh/hse-application-template

hse-gennet contains DQMH modules and helper VIs for generic networking in DQMH-based applications. Find more information at
https://gitlab.com/hampel-soft/dqmh/generic-networking

hse-lib is a collection of helpful VIs and functions for everyday-use.

hse-logger is a tool that helps with logging to various targets. It is losely based on the python logger libraries.

## :wrench: Configuration 

tbd. or n/a?


## :busts_in_silhouette: Contributing 

Please contribute! We'd love to see code coming back to this repo. Get in touch if you want to know more about contributing.

##  :beers: Credits

* Joerg Hampel
* Manuel Sebald

## :page_facing_up: License 

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details